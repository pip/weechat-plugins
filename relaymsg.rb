# frozen_string_literal: true

require 'rubygems'

# @see {https://weechat.org/files/doc/stable/weechat_plugin_api.en.html#_hook_line}
line_hashtable = %i[buffer buffer_name buffer_type y date date_printed str_time tags_count tags displayed notify_level highlight prefix message]

# Procesa el parámetro de Weechat.hook_line de forma que podamos obtener
# información sin tener que hacer malabares.
#
# TODO: Reutilizar esto en otros plugins.
Line = Struct.new(*line_hashtable, keyword_init: true) do
  def tags
    @tags ||= self[:tags].split(',')
  end

  def host
    @host ||= tags.find { |t| t.start_with?('host_') }&.sub('host_', '')
  end

  def nick
    @nick ||= tags.find { |t| t.start_with?('nick_') }&.split('_', 2)&.last
  end
end

# Detecta RELAYMSG para ir autocompletando la lista de nicks a partir de
# los mensajes que reenvíe le cyborg.
#
# Genera un grupo para cada red y va agregando los nicks a la lista.  A
# medida que van hablando desde otros lados, se va llenando la lista de
# nicks y podemos autocompletarles.
def relaymsg(_, line)
  # Usamos tap para que el valor de retorno de esta función siempre sea
  # la línea original, de otra forma Weechat crashea.
  line.tap do |l|
    line = Line.new(**l.transform_keys(&:to_sym))

    # Salir si el host incluye una @
    next unless line.host
    next unless line.nick
    # XXX: Oragono no envía un tag especial para RELAYMSG, pero ofusca
    # el host del nick con nick/red.  Si incluye una @ quiere decir que
    # es un mensaje de IRC.
    next if line.host.include? '@'

    # XXX: Esto depende de la configuración de Matterbridge
    nick, group_name = line.nick.split('/', 2)

    next unless nick
    next unless group_name

    # XXX: Usamos empty? porque el NULL de Weechat se convierte en ''
    group = Weechat.nicklist_search_group(line.buffer, '', group_name)
    group = Weechat.nicklist_add_group(line.buffer, '', group_name, 'weechat.color.nicklist_group', 1) if group.empty?

    # Agrega la @ para poder autocompletar y mencionar en la otra red.
    nick = "@#{nick}"

    if Weechat.nicklist_search_nick(line.buffer, group, nick).empty?
      Weechat.nicklist_add_nick(line.buffer, group, nick, 'weechat.color.nicklist_away', '', 'green', 1)
    end
  end
end

# Función de inicialización.
def weechat_init
  Weechat.register('relaymsg', 'f', '0.1.0', 'MIT-Antifa', 'RELAYMSG', '', '')
  Weechat.hook_line('*', '*', '*', 'relaymsg', '')

  Weechat::WEECHAT_RC_OK
end
