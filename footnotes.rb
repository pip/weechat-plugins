# frozen_string_literal: true

require 'rubygems'
require 'sequel'
require 'url_privacy'

DB = Sequel.sqlite(File.expand_path('~/.weechat/footnotes.sqlite3'))

DB.create_table? :urls do
  primary_key :id
  String :url, unique: true, null: false
end

URLS = DB[:urls]
URL_RE = %r{[<"]?(?<url>https?://[^ >"]+)[>"]?}

# Stores URLs in the database and replaces them for their IDs
def footnote(_, line)
  return line unless URL_RE =~ line['message']

  # Save original URLs and replace them with a footnote
  # @see {https://bugs.ruby-lang.org/issues/12745}
  line['message'].gsub!(URL_RE) do |url|
    id   = URLS.where(url: $1).get(:id)
    id ||= URLS.insert(url: $1)

    "[#{id}]"
  end

  line
end

# Find URL by ID and open in browser
def footnote_open(_, _, id)
  url = URLS.where(id: id.to_i).get(:url)

  # XXX: If the browser isn't open yet, it'll be opened by Weechat.
  system({'DISPLAY' => ':0'}, 'xdg-open', UrlPrivacy.clean(url)) if url

  Weechat::WEECHAT_RC_OK
end

def weechat_init
  Weechat.register('footnotes', 'f', '0.1.0', 'MIT-Antifa', 'Footnotes', '', '')
  Weechat.hook_line('*', '*', '*', 'footnote', '')
  Weechat.hook_command('footnote', 'Open footnote', '[id]', 'id: id of URL as seen between [brackets]', '', 'footnote_open', '')

  Weechat.print("", $:.inspect)

  Weechat::WEECHAT_RC_OK
end
