# Weechat

Algunos complementos para Weechat :D

# Instalación

Clonar el repositorio:

```bash
cd ~/Proyectos # o donde guardes tus repositorios
git clone https://0xacab.org/pip/weechat-plugins.git
```

Vincular al directorio de Weechat, reemplazando `PLUGIN` por el nombre
del plugin a instalar:

```bash
cd ~/Proyectos/weechat-plugins
ln -sv $PWD/PLUGIN.rb ~/.weechat/ruby/autoload/
```

De esta forma siempre van a estar actualizados con respecto al
repositorio.

Para cargar los plugins sin reiniciar Weechat:

```
/ruby load ruby/autoload/PLUGIN.rb
```

# Footnotes

Convierte las URLs en números y las guarda en una base de datos para
poder abrirlas directamente en el navegador.  Con el comando `/footnote
NUMERO` podemos abrir la URL sin tener que usar el mouse para
cliquearla.

Además evita la contaminación visual y elimina parámetros de _tracking_.

```bash
# Instalar las dependencias antes de habilitar el plugin.
gem install sequel
gem install url_privacy
```

# Relaymsg

Detecta los mensajes que vienen desde otras redes de mensajería y agrega
los nombres a la lista de nicks, para que podamos usar autocompletado.
